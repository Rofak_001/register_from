import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import ItemModal from './components/ItemModal';
import ItemLogin from './components/ItemLogin';
function App() {
  return (
    <div className="App">
        <ItemModal/>
        <ItemLogin/>
    </div>
  );
}

export default App;
