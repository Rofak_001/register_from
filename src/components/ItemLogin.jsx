import React, { Component } from 'react';
import { Button, Modal, Form, Container } from 'react-bootstrap';
import '../Styles/ModalStyle.css';
import '../Styles/LoginStyle.css';
import CancelImg from '../Image/cancle.png';
import ReactFacebookLogin from 'react-facebook-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faExclamationCircle } from '@fortawesome/free-solid-svg-icons';

export default class ItemModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			email: '',
			pass: '',
		};
		this.handleChange = this.handleChange.bind(this);
	}
	handleShow = () => {
		this.setState({ show: true });
	};
	handleClose = () => {
		this.setState({ show: false, email: '', pass: '' });
	};
	responseFacebook = (response) => {
		console.log(response);
	};
	handleClick = () => {
		//Email
		const email = document.getElementById('email');
		const errorIcon = document.getElementById('errorIcon');
		const text = document.getElementById('textErorr');
		//Password
		const pass = document.getElementById('pass');
		const errorIconPass = document.getElementById('errorIconPass');
		const textErorrPass = document.getElementById('textErorrPass');
		if (this.state.email !== '') {
			if (!isEmail(this.state.email)) {
				setErrorFor(email, errorIcon, text, 'Not a valid email!');
			} else {
				//code
				setSuccessFor(email, errorIcon, text, '');
			}
		} else {
			setErrorFor(email, errorIcon, text, 'Email Cannot Empty!');
		}
		if (this.state.pass !== '') {
			//code
			setSuccessFor(pass, errorIconPass, textErorrPass, '');
		} else {
			setErrorFor(pass, errorIconPass, textErorrPass, 'Password Cannot Empty!');
		}
	};
	handleChange = (event) => {
		const value = event.target.value;
		const name = event.target.name;
		this.setState({
			[name]: value,
		});
	};
	render() {
		return (
			<div>
				<Container className="text-center">
					<Button variant="primary" onClick={this.handleShow.bind(this)}>
						Login
					</Button>
					<Modal
						aria-labelledby="contained-modal-title-vcenter"
						centered
						show={this.state.show}
						onHide={this.handleClose.bind(this)}
					>
						<img
							className="btnClose"
							onClick={this.handleClose.bind(this)}
							src={CancelImg}
							alt="Cancle"
							width="15px"
						/>
						<Modal.Body>
							<h4 className="tRegister">Login</h4>
							<hr className="line" />
							<Form.Group className="wrap_error mb-0">
								<Form.Label>អុីម៉ែល</Form.Label>
								<Form.Control
									type="email"
									placeholder="អុីម៉ែល"
									className="InputfrmRegister form-rounded"
									id="email"
									name="email"
									onChange={(event) => this.handleChange(event)}
								/>
								<FontAwesomeIcon icon={faExclamationCircle} className="error" id="errorIcon" />
								<Form.Label className="small" id="textErorr"></Form.Label>
							</Form.Group>
							<Form.Group className="wrap_error">
								<Form.Label>លេខសម្ងាត់</Form.Label>
								<Form.Control
									type="password"
									placeholder="លេខសម្ងាត់"
									className="InputfrmRegister form-rounded"
									name="pass"
									id="pass"
									onChange={(event) => this.handleChange(event)}
								/>
								<FontAwesomeIcon icon={faExclamationCircle} className="error" id="errorIconPass" />
								<Form.Label className="small" id="textErorrPass"></Form.Label>
							</Form.Group>
							<Button
								variant="primary form-control form-rounded mt-2"
								onClick={this.handleClick.bind(this)}
							>
								ចូលប្រើ
							</Button>
							<FacebookLogin
								appId="1357467214445401"
								callback={this.responseFacebook}
								render={(renderProps) => (
									<Button
										className="form-control form-rounded primary mt-2"
										onClick={renderProps.onClick}
									>
										<span>
											<FontAwesomeIcon icon={faFacebookF} className="mr-2" />
										</span>{' '}
										ចូលប្រើតាម​ facebook
									</Button>
								)}
							/>
						</Modal.Body>
					</Modal>
				</Container>
			</div>
		);
	}
}
function setErrorFor(input, errorIcon, text, message) {
	input.classList.add('input_error');
	errorIcon.classList.remove('error');
	errorIcon.classList.add('errorShow');
	text.innerText = message;
}
function setSuccessFor(input, errorIcon, text, message) {
	input.classList.remove('input_error');
	errorIcon.classList.remove('errorShow');
	errorIcon.classList.add('error');
	text.innerText = message;
}
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
		email
	);
}
