import React, { Component } from 'react';
import { Button, Modal, Form, Container } from 'react-bootstrap';
import '../Styles/ModalStyle.css';
import '../Styles/LoginStyle.css';
import CancelImg from '../Image/cancle.png';
import { faExclamationCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
export default class ItemModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			username: '',
			email: '',
			pass: '',
			passComfirm: '',
		};
		this.handleChange = this.handleChange.bind(this);
	}
	handleShow = () => {
		this.setState({ show: true });
	};
	handleClose = () => {
		this.setState({ show: false, username: '', email: '', pass: '', passComfirm: '' });
	};
	handleClick = () => {
		//username
		const username = document.getElementById('username');
		const errorIconForUser = document.getElementById('errorIconForUser');
		const textErorrUser = document.getElementById('textErorrUser');
		//Email
		const email = document.getElementById('email');
		const errorIconForEmail = document.getElementById('errorIconForEmail');
		const textErorrEmail = document.getElementById('textErorrEmail');
		//Password
		const pass = document.getElementById('pass');
		const errorIconForPass = document.getElementById('errorIconForPass');
		const textErorrPass = document.getElementById('textErorrPass');
		//Comfirm Password
		const passComfirm = document.getElementById('passComfirm');
		const errorIconForPassComfirm = document.getElementById('errorIconForPassComfirm');
		const textErorrPassComfirm = document.getElementById('textErorrPassComfirm');
		//validation for username
		if (this.state.username !== '') {
			//code
			setSuccessFor(username, errorIconForUser, textErorrUser, '');
		} else {
			setErrorFor(username, errorIconForUser, textErorrUser, 'Username Cannot Empty!');
		}
		//validation for email
		if (this.state.email !== '') {
			if (!isEmail(this.state.email)) {
				setErrorFor(email, errorIconForEmail, textErorrEmail, 'Not a valid email!');
			} else {
				//code
				setSuccessFor(email, errorIconForEmail, textErorrEmail, '');
			}
		} else {
			setErrorFor(email, errorIconForEmail, textErorrEmail, 'Email Cannot Empty!');
		}
		//Validatin for password
		if (this.state.pass !== '') {
			setSuccessFor(pass, errorIconForPass, textErorrPass, '');
		} else {
			setErrorFor(pass, errorIconForPass, textErorrPass, 'Password Cannot Empty!');
		}
		//validation for Comfrim password
		if (this.state.passComfirm !== '') {
			setSuccessFor(passComfirm, errorIconForPassComfirm, textErorrPassComfirm, '');
		} else {
			setErrorFor(passComfirm, errorIconForPassComfirm, textErorrPassComfirm, 'Comfrim Password Cannot Empty!');
		}
	};
	handleChange = (event) => {
		//Comfirm Password
		const passComfirm = document.getElementById('passComfirm');
		const errorIconForPassComfirm = document.getElementById('errorIconForPassComfirm');
		const textErorrPassComfirm = document.getElementById('textErorrPassComfirm');
		const value = event.target.value;
		const name = event.target.name;
		if (name === 'passComfirm') {
			if (value === this.state.pass) {
				this.setState({
					[name]: value,
				});
				setSuccessFor(passComfirm, errorIconForPassComfirm, textErorrPassComfirm, '');
			} else {
				setErrorFor(passComfirm, errorIconForPassComfirm, textErorrPassComfirm, 'Password Not Match!');
			}
		} else {
			this.setState({
				[name]: value,
			});
		}
	};
	render() {
		return (
			<div>
				<Container className="text-center">
					<Button variant="primary" onClick={this.handleShow.bind(this)}>
						Register
					</Button>
					<Modal
						aria-labelledby="contained-modal-title-vcenter"
						centered
						show={this.state.show}
						onHide={this.handleClose.bind(this)}
					>
						<img
							className="btnClose"
							onClick={this.handleClose.bind(this)}
							src={CancelImg}
							alt="Cancle"
							width="15px"
						/>
						<Modal.Body>
							<h4 className="tRegister">Register New</h4>
							<hr className="line" />
							{/* username */}
							<Form.Group className="wrap_error mb-0">
								<Form.Label>នាមត្រកូល​ និង​ នាមខ្លួន</Form.Label>
								<Form.Control
									type="username"
									placeholder="ឈ្មោះ"
									className="InputfrmRegister form-rounded"
									id="username"
									name="username"
									onChange={(event) => this.handleChange(event)}
								/>
								<FontAwesomeIcon icon={faExclamationCircle} className="error" id="errorIconForUser" />
								<Form.Label className="small" id="textErorrUser"></Form.Label>
							</Form.Group>
							{/* email */}
							<Form.Group className="wrap_error mb-0">
								<Form.Label>អុីម៉ែល</Form.Label>
								<Form.Control
									type="email"
									placeholder="អុីម៉ែល"
									className="InputfrmRegister form-rounded"
									id="email"
									name="email"
									onChange={(event) => this.handleChange(event)}
								/>
								<FontAwesomeIcon icon={faExclamationCircle} className="error" id="errorIconForEmail" />
								<Form.Label className="small" id="textErorrEmail"></Form.Label>
							</Form.Group>
							{/* password */}
							<Form.Group className="wrap_error mb-0">
								<Form.Label>លេខសម្ងាត់</Form.Label>
								<Form.Control
									type="password"
									placeholder="លេខសម្ងាត់"
									className="InputfrmRegister form-rounded"
									id="pass"
									name="pass"
									onChange={(event) => this.handleChange(event)}
								/>
								<FontAwesomeIcon icon={faExclamationCircle} className="error" id="errorIconForPass" />
								<Form.Label className="small" id="textErorrPass"></Form.Label>
							</Form.Group>
							{/* comfirm pass*/}
							<Form.Group className="wrap_error">
								<Form.Label>បញ្ជាក់លេខសម្ងាត់</Form.Label>
								<Form.Control
									type="password"
									placeholder="បញ្ជាក់លេខសម្ងាត់"
									id="passComfirm"
									name="passComfirm"
									className="InputfrmRegister form-rounded"
									onChange={(event) => this.handleChange(event)}
								/>
								<FontAwesomeIcon
									icon={faExclamationCircle}
									className="error"
									id="errorIconForPassComfirm"
								/>
								<Form.Label className="small" id="textErorrPassComfirm"></Form.Label>
							</Form.Group>
							<Button
								onClick={this.handleClick.bind(this)}
								variant="primary form-control form-rounded mt-3"
							>
								ដាក់ស្នើរ
							</Button>
						</Modal.Body>
					</Modal>
				</Container>
			</div>
		);
	}
}
function setErrorFor(input, errorIcon, text, message) {
	input.classList.add('input_error');
	errorIcon.classList.remove('error');
	errorIcon.classList.add('errorShow');
	text.innerText = message;
}
function setSuccessFor(input, errorIcon, text, message) {
	input.classList.remove('input_error');
	errorIcon.classList.remove('errorShow');
	errorIcon.classList.add('error');
	text.innerText = message;
}
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
		email
	);
}
